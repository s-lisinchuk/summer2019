﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Utils;
using VkNet.Utils.AntiCaptcha;
using VkNet.Model.RequestParams;
using SkBot.utilities;
using System.Text.RegularExpressions;
using System.Globalization;
using Google.Maps.Places;
using System.Net;
using System.IO;

namespace SkBot
{
    class Users : MessageProcessor
    {
        VkApiWithQuery helper;
        public Users(ref VkApiWithQuery _vk, ref VkApiWithQuery _helper) : base(ref _vk)
        {
            helper = _helper;
            this.addCommand("Команды", commListCommand, new List<String> { "Команда", "Список команд", "К", "R" }, "выводит список доступных команд");
            this.addCommand("Привет", privet, new List<String> { "Даров", "прив", "ку", "hi" });
            this.addCommand("Пока", poka, new List<String> { "До свидания", "Лан пока", "Bye" });
            this.addCommand("Хочу музыку", muz, new List<string> { "Музыка", "Скинь музыку", "Хочу музон" });
        }

        public override bool processMessage(VkNet.Model.Message message)
        {
            if (!processCommand(message))
            {
                vkapi.sendMessageToQuery(new MessagesSendParams
                {
                    UserId = message.FromId,
                    Message =  "Такой команды не существует"
                });
            }
            else
                return true;


            return false;
        }

        public void commListCommand(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            StringBuilder str = new StringBuilder("Вы можете воспользоваться этими командами. Для этого напишите мне команду или первую букву команды:\r\n\r\n");
            foreach (String name in this.getCommands()) str.AppendLine(name);
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = str.ToString(),
            });
            messageThread.fsm_state = 0;
        }

        public void privet(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            String reply = "";
                    reply = "И тебе привет";
                    
                    messageThread.fsm_state = 0;
        
        
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply,
            });
        }

        public void poka(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            var users = vkapi.vkApi.Users.Get(new long[] { (long)message.FromId });
            
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = "Пока, " + users[0].FirstName,
            });
            messageThread.fsm_state = 0;
        }
        public void muz(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            String reply = "";
            string path = "";
            
            switch (messageThread.fsm_state)
            {
                case 0:
                    reply = "Какой жанр музыки тебе нравится? Выбери из списка.";
                    path = Directory.GetCurrentDirectory() + "\\genres.txt";
                    reply = File.ReadAllText(path, Encoding.Default);
                    messageThread.fsm_state = 1;
                    break;
                case 1:
                    Random r = new Random();
                    path = Directory.GetCurrentDirectory() + "\\music\\" + message.Text + ".txt";
                    if (!File.Exists(path))
                    {
                        reply = "Данного жанра музыки не существует.";
                    }
                    else
                    {
                        
                        reply = "Подбирается музыка с помощью рандома...\r\n";
                        var s = File.ReadAllText(path, Encoding.Default);
                        String[] tracks = s.Split(new char[] {'\n'});
                        reply += tracks[r.Next(tracks.Length - 1)];
                    }
                    messageThread.fsm_state = 0;
                    break;
            }
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
            
                UserId = message.FromId,
                Message = reply,
            });
        }
    }     
}
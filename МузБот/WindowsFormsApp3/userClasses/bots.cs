﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Utils;
using VkNet.Utils.AntiCaptcha;
using VkNet.Model.RequestParams;
using SkBot.utilities;
using System.Globalization;
using OfficeOpenXml;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Text.RegularExpressions;


namespace SkBot
{
    class Bots 
    {
        VkApiWithQuery mainVK;
        VkApiWithQuery helper;
        Users userproc;
        public Bots(ref VkApiWithQuery _mainVK, ref VkApiWithQuery _helper)
        {
            mainVK = _mainVK;
            helper = _helper;
            userproc = new Users(ref _mainVK, ref _helper);
        }

        public bool processMessage(VkNet.Model.Message message)
        {
            return userproc.processMessage(message);
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Model.RequestParams;
using VkNet.Utils;
using System.IO;
using System.Net;
using System.Net.Mail;
using OfficeOpenXml;
using SkBot.utilities;
using NLog;
using System.Collections.ObjectModel;

namespace SkBot
{
    public partial class Form1 : Form
    {

        VkNet.VkApi mainapi;
        VkApiWithQuery mainApi;
        Bots bot;
        VkNet.Model.LongPollServerResponse response;
        private BackgroundWorker backgroundMessages;

        public Form1()
        {
            InitializeComponent();
             
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            mainapi = new VkNet.VkApi();
            mainApi = new VkApiWithQuery(20, ref mainapi);
            mainApi.vkApi.RequestsPerSecond = 20;
            bot = new Bots(ref mainApi);
            backgroundMessages = new BackgroundWorker();
            backgroundMessages.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
        }

        private void MessageReadTimer_Tick(object sender, EventArgs e)
        {
            //For future use
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            VkNet.Model.LongPollHistoryResponse resp = mainApi.vkApi.Messages.GetLongPollHistory(new MessagesGetLongPollHistoryParams
            {
                Ts = response.Ts,
                Pts = response.Pts
            });
            while (true)
            {
                try
                {
                    if (CheckForInternetConnection())
                    {
                        resp = mainApi.vkApi.Messages.GetLongPollHistory(new MessagesGetLongPollHistoryParams
                        {
                            Pts = resp.NewPts
                        });
                        
                        foreach (VkNet.Model.Message message in resp.Messages)//get.Messages.Reverse())
                        {
                            if (message.Type == VkNet.Enums.MessageType.Received)
                            {
                                bot.processMessage(message);
                            }
                        }
                        mainApi.processQuery();
                    }
                    if (backgroundMessages.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                }
                catch(Exception ex)
                {
                   if (ex.Message.Contains("token")) mainApi.vkApi.RequestsPerSecond = 20;

                }
            }
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            if (CheckForInternetConnection())
            {
                if (loginButton.Text == "Login")
                {
                    if (mainTokenTextBox.Text != "")
                    {
                        if (!mainApi.vkApi.IsAuthorized)
                        {
                            Settings scope = Settings.Messages | Settings.Wall | Settings.Friends | Settings.Photos | Settings.Documents | Settings.Offline;      // Приложение имеет доступ к друзьям
                            try
                            {
                                mainApi.vkApi.Authorize(new ApiAuthParams
                                {
                                    Settings = scope,
                                    AccessToken = mainTokenTextBox.Text
                                });
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Error: Login error: " + ex.Message);
                                return;
                            }
                            response = mainApi.vkApi.Messages.GetLongPollServer(true);
                        }
                        backgroundMessages.RunWorkerAsync();
                        loginButton.Text = "Logout";
                    }
                    else
                    {
                        MessageBox.Show("Заполните поля для авторизации");
                    }
                }
                else
                {
                    backgroundMessages.CancelAsync();
                    loginButton.Text = "Login";
                }
            }
        }

        private static bool CheckForInternetConnection()
        {
            try
            {
                using (var  client = new WebClient())
                {
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        private void mainTokenTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

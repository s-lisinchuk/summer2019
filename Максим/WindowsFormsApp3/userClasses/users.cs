﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Utils;
using VkNet.Utils.AntiCaptcha;
using VkNet.Model.RequestParams;
using SkBot.utilities;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;

namespace SkBot
{
    class Users : MessageProcessor
    {

        public Users(ref VkApiWithQuery _vk) : base(ref _vk)
        {
            this.addCommand("Команды", commListCommand, new List<String> { "Команда", "Список команд", "К", "R" }, "выводит список доступных команд");
            this.addCommand("Привет", privet);
            this.addCommand("Дата рождения", privet);
            
        }

        public override bool processMessage(VkNet.Model.Message message)
        {
            if (!processCommand(message))
            {
                vkapi.sendMessageToQuery(new MessagesSendParams
                {
                    UserId = message.FromId,
                  Message = "Такой команды не существует"
                   
                });
            }
            else
                return true;


            return false;
        }

        public void commListCommand(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            StringBuilder str = new StringBuilder("Вы можете воспользоваться этими командами. Для этого напишите мне команду или первую букву команды:\r\n\r\n");
            foreach (String name in this.getCommands()) str.AppendLine(name);
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = str.ToString(),
            });
            messageThread.fsm_state = 0;
        }

        public void privet(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            var users = vkapi.vkApi.Users.Get(new long[] { (long)message.FromId }, ProfileFields.BirthDate);
            String reply = "АЙАЙАЙАЙАЙАЙАЙАЙАЙАЙА";
            DateTime date; String site = "";
            switch (messageThread.fsm_state)
            {
                case 0:
                    if (users[0].BirthDate != null)
                    {
                        reply = "Ты родился " + users[0].BirthDate;
                        messageThread.fsm_state = 0;
                       

                        date = DateTime.ParseExact(users[0].BirthDate, "d.M.yyyy", null);
                        
                        if (date.DayOfYear > DateTime.ParseExact("21.03", "dd.MM", null).DayOfYear && date.Date.DayOfYear < DateTime.ParseExact("19.04", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты овен. Вот твоё предсказание:\r\n";
                            site = "https://horo.mail.ru/prediction/aries/today/";
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("20.04", "dd.MM", null).DayOfYear && date.Date.DayOfYear < DateTime.ParseExact("20.05", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты телец. Вот твоё предсказание:\r\n ";
                            site = "https://horo.mail.ru/prediction/taurus/today/";
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("21.05", "dd.MM", null).DayOfYear && date.Date.DayOfYear < DateTime.ParseExact("20.06", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты близнецы. Вот твоё предсказание:\r\n ";
                            site = "https://horo.mail.ru/prediction/gemini/today/";

                        }
                        else if (date.DayOfYear > DateTime.ParseExact("21.06", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("22.07", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты рак. Вот твоё предсказание:\r\n ";
                            site = "https://horo.mail.ru/prediction/cancer/today/";
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("23.07", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("22.08", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты лев. Вот твоё предсказание:\r\n"; 
                            site = "https://horo.mail.ru/prediction/leo/today/";
                            
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("23.08", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("22.09", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты дева. Вот твоё предсказание:\r\n"; 
                            site = "https://horo.mail.ru/prediction/virgo/today/";
                         
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("23.09", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("22.10", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты весы. Вот твоё предсказание:\r\n";
                            site = "https://horo.mail.ru/prediction/libra/today/";

                        }
                        else if (date.DayOfYear > DateTime.ParseExact("23.10", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("21.11", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты  скорпион. Вот твоё предсказание:\r\n";
                            site = "https://horo.mail.ru/prediction/scorpio/today/";
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("22.11", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("21.12", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты стрелец. Вот твоё предсказание:\r\n";
                            site = "https://horo.mail.ru/prediction/sagittarius/today/";
                            
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("22.12", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("20.01", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты козерог. Вот твоё предсказание:\r\n";
                            site = "https://horo.mail.ru/prediction/capricorn/today/";
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("20.01", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("18.02", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты водолей. Вот твоё предсказание:\r\n";
                            site = "https://horo.mail.ru/prediction/aquarius/today/";
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("19.02", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("20.03", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты рыба. Вот твоё предсказание:\r\n";
                            site = "https://horo.mail.ru/prediction/pisces/today/";
                        }

                        WebRequest request = WebRequest.Create(site);
                        request.Method = "GET";
                        using (WebResponse response = request.GetResponse())
                        {
                            using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                            {
                                string s = sr.ReadToEnd();
                                Match m = Regex.Match(s, "article__item article__item_alignment_left article__item_html\"[>][<]p[>].+[<]\\/p[>]");
                                reply += m.Value.Replace("article__item article__item_alignment_left article__item_html\"><p>", "").
                                    Replace("</p>", "");
                            }
                        }
                    }
                    else
                    {
                        reply = "Когда ты родился? Введи дату в формате ДД.ММ.ГГ";
                        messageThread.fsm_state = 1;
                        

                    }
                    break;
                case 1:
                    try
                    {
                        date = DateTime.ParseExact(message.Text, "dd.MM.yy", null);
                        messageThread.fsm_state = 0;

                        if (date.DayOfYear > DateTime.ParseExact("21.03", "dd.MM", null).DayOfYear && date.Date.DayOfYear < DateTime.ParseExact("19.04", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты овен. Вот твоё предсказание:\r\n"; 
                            site = "https://horo.mail.ru/prediction/aries/today/";
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("20.04", "dd.MM", null).DayOfYear && date.Date.DayOfYear < DateTime.ParseExact("20.05", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты телец. Вот твоё предсказание:\r\n"; 
                            site = "https://horo.mail.ru/prediction/taurus/today/";
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("21.05", "dd.MM", null).DayOfYear && date.Date.DayOfYear < DateTime.ParseExact("20.06", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты близнецы. Вот твоё предсказание:\r\n"; 
                            site = "https://horo.mail.ru/prediction/gemini/today/";
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("21.06", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("22.07", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты рак. Вот твоё предсказание:\r\n"; 
                            site = "https://horo.mail.ru/prediction/cancer/today/";
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("23.07", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("22.08", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты лев. Вот твоё предсказание:\r\n";
                            site = "https://horo.mail.ru/prediction/leo/today/";
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("23.08", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("22.09", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты дева. Вот твоё предсказание:\r\n";
                            site = "https://horo.mail.ru/prediction/virgo/today/";
                        }
                        else if (date.DayOfYear > DateTime.ParseExact("23.09", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("22.10", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты весы. Вот твоё предсказание:\r\n";
                            site = "https://horo.mail.ru/prediction/libra/today/";

                        }
                        else if (date.DayOfYear > DateTime.ParseExact("23.10", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("21.11", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты  скорпион. Вот твоё предсказание:\r\n"; 
                            site = "https://horo.mail.ru/prediction/scorpio/today/";

                        }
                        else if (date.DayOfYear > DateTime.ParseExact("22.11", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("21.12", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты стрелец. Вот твоё предсказание:\r\n"; 
                            site = "https://horo.mail.ru/prediction/sagittarius/today/";

                        }
                        else if (date.DayOfYear > DateTime.ParseExact("22.12", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("20.01", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты козерог. Вот твоё предсказание:\r\n";
                            site = "https://horo.mail.ru/prediction/capricorn/today/";

                        }
                        else if (date.DayOfYear > DateTime.ParseExact("20.01", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("18.02", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты водолей. Вотвот твоё предсказание:\r\n"; 
                            site = "https://horo.mail.ru/prediction/aquarius/today/";

                        }
                        else if (date.DayOfYear > DateTime.ParseExact("19.02", "dd.MM", null).DayOfYear && date.DayOfYear < DateTime.ParseExact("20.03", "dd.MM", null).DayOfYear)
                        {
                            reply = "Ты рыба. Вот твоё предсказание:\r\n"; 
                            site = "https://horo.mail.ru/prediction/pisces/today/";

                        }
                        WebRequest request = WebRequest.Create(site);
                        request.Method = "GET";
                        using (WebResponse response = request.GetResponse())
                        {
                            using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                            {
                                string s = sr.ReadToEnd();
                                Match m = Regex.Match(s, "article__item article__item_alignment_left article__item_html\"[>][<]p[>].+[<]\\/p[>]");
                                reply += m.Value.Replace("article__item article__item_alignment_left article__item_html\"><p>", "").
                                    Replace("</p>", "");
                            }
                        }
                    }
                    catch
                    {
                        reply = "Вы несёте какой-то бред";
                    }
                    
                    messageThread.fsm_state = 0;
                    break;
            }
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply,
            });
        }
    }
}
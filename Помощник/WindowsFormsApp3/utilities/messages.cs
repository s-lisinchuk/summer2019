﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Utils;
using VkNet.Utils.AntiCaptcha;
using VkNet.Model.RequestParams;
using SkBot.utilities;

namespace SkBot
{
    abstract class MessageProcessor
    {
        protected Commandsclass commands;
        protected VkApiWithQuery vkapi;
        protected MessageProcessor(ref VkApiWithQuery _vkapi)
        {
            vkapi = _vkapi;
            commands = new Commandsclass();
        }

        public abstract bool processMessage(VkNet.Model.Message message);
        public void addCommand(String _commandname, Commandsclass.commandReceivedCallback _callback)
        {
            commands.addCommand(_commandname, _callback);
        }
        public void addCommand(String _commandname, Commandsclass.commandReceivedCallback _callback, List<String> synonyms)
        {
            commands.addCommand(_commandname, _callback, synonyms);
        }
        public void addCommand(String _commandname, Commandsclass.commandReceivedCallback _callback, List<String> synonyms, String description)
        {
            commands.addCommand(_commandname, _callback, synonyms, description);
        }
        protected bool processCommand(VkNet.Model.Message message)
        {
            while (message.Text.Contains("  ")) message.Text = message.Text.Replace("  ", " ");
            return (this.commands.parseMessageText(message));
        }
        public List<String> getCommands()
        {
            return commands.getCommands();
        }
    }
}

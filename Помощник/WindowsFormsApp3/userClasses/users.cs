﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Utils;
using VkNet.Utils.AntiCaptcha;
using VkNet.Model.RequestParams;
using SkBot.utilities;
using System.Text.RegularExpressions;
using System.Globalization;
using VkNet.Model.Keyboard;
using System.Windows.Forms;//MessageBox
using System.IO;
using System.Text.RegularExpressions;
using System;
using System.Threading;


namespace SkBot
{
    class Users : MessageProcessor
    {
        DateTime last_used_time;
        public Users(ref VkApiWithQuery _vk) : base(ref _vk)
        {
            this.addCommand("Команды", commListCommand, new List<String> { "Команда"}, "выводит список доступных команд");
            this.addCommand("Привет", privet, new List<String> {"Приветули", "Приветик", "Хай", "Здравствуй"});
            this.addCommand("Пока", poka, new List<String> { "Прощай", "Поки"});
            this.addCommand("Расписание", racpicanie);
            this.addCommand("Игры", games);
            this.addCommand("Загрузить расписание", racpicaniee);
        }

        public override bool processMessage(VkNet.Model.Message message)
        {
            if (!processCommand(message))
            {
                vkapi.sendMessageToQuery(new MessagesSendParams
                {
                    UserId = message.FromId,
                    Message =  "Такой команды не существует"
                });
            }
            else
                return true;


            return false;
        }

        public void commListCommand(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            StringBuilder str = new StringBuilder("Вы можете воспользоваться этими командами. Для этого напишите мне команду или первую букву команды:\r\n\r\n");
            foreach (String name in this.getCommands()) str.AppendLine(name);
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = str.ToString(),
            });
            messageThread.fsm_state = 0;
        }

        public void privet(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = "Привет",
            });
            messageThread.fsm_state = 0;
        }
        public void poka(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = "Пока",
            });
            messageThread.fsm_state = 0;
        }
        public void racpicanie(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            String reply = "";
            switch (messageThread.fsm_state)
            {
                case 0:
                    messageThread.fsm_state = 1;
                    reply = "Введите данные, для составления расписания. Уточните время, при желании можете поставить данную функцию на таймер, чтобы я вам напомнил";
                        break;
                case 1:
                    messageThread.fsm_state = 2;
                    messageThread.arg = message.Text;
                    reply = "На какое время поставить таймер? Если таймер ставить не надо, напиши Отмена";
                    break;
                case 2:
                    if (message.Text.ToLower() == "отмена")
                    {
                        reply = "Расписание загруженно без таймера";
                        
                        String s = Directory.GetCurrentDirectory();
                        using (StreamWriter sw = File.CreateText(s + "\\расписание.txt"))
                        {
                            sw.WriteLine(messageThread.arg as string);
                        }
                    }
                    else
                    {
                        try
                        {
                            Match m = Regex.Match(message.Text, "\\d{2}:\\d{2}:\\d{2}");
                            DateTime Data = DateTime.ParseExact(m.Value, "HH:mm:ss", null);
                            reply = "Расписание загруженно с Таймером" + Data.ToString("HH:mm:ss");
                            String s = Directory.GetCurrentDirectory();
                            using (StreamWriter sw = File.AppendText(s + "\\напоминалки.txt"))
                            {
                                sw.WriteLine(Data.ToString("HH:mm:ss") + "---" + message.FromId.ToString() + "---" + (messageThread.arg as string));
                            }
                        }
                        catch
                        {
                            reply = "Вы прислали неверное время";
                        }
                    }
                    messageThread.fsm_state = 0;
                    break;

            }
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply,
            });
        }
        public void games(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            
            String reply = "";
            float dmg1 = 30f;
            float dmg2 = 30f;
            float healf1 = 200f;
            float healf2 = 200f;
            float def1 = 15f;
            float def2 = 15f;
            bool check = false;
            if (messageThread.arg != null) check = (bool)messageThread.arg;
            MessageKeyboard keyboard = null;
            List<string> keys = new List<string>();
            switch (messageThread.fsm_state)
            {
                case 0:
                    messageThread.fsm_state = 1;
                    reply = "Сайт игр или игра со мной?(нужно написать сайт или с тобой)";
                    break;
                case 1:
                    messageThread.fsm_state = 2;
                    if (message.Text.ToLower() == "сайт")
                    {
                        reply = "https://gamejolt.com/";
                        messageThread.fsm_state = 0;
                    }
                    else if (message.Text.ToLower() == "с тобой")
                    {
                        reply = "Игра началась,ваш ход! Ва можете писать Атака,Лечение и Защита чтобы сделать эти действия ";
                        messageThread.fsm_state = 2;
                    }
                    break;
                case 2:
                    if (!check)
                    {
                        messageThread.fsm_state = 3;
                        if (message.Text.ToLower() == "атака")
                        {
                            healf2 = healf2 - dmg1;
                            reply = ("У меня осталось" + healf2.ToString());
                            check = true;
                            messageThread.fsm_state = 3;
                        }
                        else if ((message.Text.ToLower() == "защита"))
                        {
                            def1 = def1 + 10;
                            dmg2 = dmg2 - (def1 + 3);
                            reply = ("Ваша защита повысилась на 13");
                            check = true;
                            messageThread.fsm_state = 3;
                        }
                        else if (message.Text.ToLower() == "лечение, ваше здоровье становиться равным" + healf1.ToString())
                        {
                            if (healf1 > 200)
                            {
                                reply = ("Ваша здоровье не может быть больше 200 единиц");

                            }
                            else if (healf1 < 200)
                            {
                                healf1 = healf1 + 24;
                                check = true;
                                reply = ("Ваша здоровье повысилось на 24");
                                messageThread.fsm_state = 3;
                            }
                        }
                       
                    }
                    break;
                case 3:
                     if(check)
                    {
                        if(healf2 >= 60)
                        {
                            healf1 = healf1 - dmg2;
                            reply = "Я атакую, у вас осталось" + healf1.ToString();
                            check = false;
                            messageThread.fsm_state = 2;
                        }
                        else if(healf2 <= 60)
                        {
                            healf2 = healf2 + 15;
                            reply = "Я полечился на 15, ваш ход!";
                            check = false;
                            messageThread.fsm_state = 2;
                        }
                    }
                    break;
            }
            messageThread.arg = check;
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply,
                Keyboard = keyboard
            });
        }

        public bool DoAnIteration()

        {
            String reply = "";
            String left_text = "";
            if ((DateTime.Now - last_used_time).TotalSeconds > 10)
            {
                last_used_time = DateTime.Now;

                string path = Directory.GetCurrentDirectory() + "\\напоминалки.txt";
                DateTime time;
                using (StreamReader sr = File.OpenText(path))
                {
                    string s;
                    while ((s = sr.ReadLine()) != null)
                    {
                        try
                        {
                            var splits = s.Split(new string[] { "---" }, StringSplitOptions.RemoveEmptyEntries);
                            time = DateTime.ParseExact(splits[0], "HH:mm:ss", null);
                            if (time.TimeOfDay <= DateTime.Now.TimeOfDay)
                            {
                                vkapi.sendMessageToQuery(new MessagesSendParams
                                {
                                    UserId = Convert.ToInt32(splits[1]),
                                    Message = splits[2]
                                });
                            }
                            else
                            {
                                left_text += s;
                            }
                        }
                        catch
                        {
                            //u
                        }
                    }
                }
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(left_text);
                }
            }
            return false;
        }


        public MessageKeyboard ListToKeyboard(List<String> list, bool one_time)
        {
            if (list.Count > 40) return null;

            int i = 1;
            double lines_counter;
            var kbuilder = new KeyboardBuilder(one_time);
            if (list.Count < 5)
            {
                lines_counter = 1;
            }
            else
            {
                lines_counter = Math.Ceiling((double)list.Count / 10);
                lines_counter = (lines_counter == 1) ? 2 : lines_counter;
            }
            foreach (string line in list)
            {
                kbuilder.AddButton(line, i.ToString());
                if (i % lines_counter == 0) kbuilder.AddLine();
                i++;
            }
            return kbuilder.Build();
        }
        public void racpicaniee(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            String reply = "";
            switch (messageThread.fsm_state)
            {
                case 0:
                    messageThread.fsm_state = 1;
                    reply = "Загрузите расписание, чтобы я его запомнил";
                    messageThread.fsm_state = 1;
                    break;
                case 1:
                    messageThread.fsm_state = 1;
                    reply = "Расписание загружено";
                    messageThread.fsm_state = 0;
                    break;
            }
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply,
            });
        }



    }
}

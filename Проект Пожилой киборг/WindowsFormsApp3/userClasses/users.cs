﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Utils;
using VkNet.Utils.AntiCaptcha;
using VkNet.Model.RequestParams;
using SkBot.utilities;
using System.Text.RegularExpressions;
using System.Globalization;
using System.IO;
using OfficeOpenXml;
using System.Net;
using System.Net.Mail;

namespace SkBot
{
    class Users : MessageProcessor
    {

        string reply = "";
        public Users(ref VkApiWithQuery _vk) : base(ref _vk)
        {
            this.addCommand("Команды", commListCommand, new List<String> { "Команда", "Список команд", "К", "R" }, "выводит список доступных команд");
            this.addCommand("Сохранить", save, new List<String> { "Сохранение", "Сохрани", "С", "C" }, "сохраняет прогресс");
            this.addCommand("Статистика", stata, new List<String> { "Стата", "Инфа", "Информация" }, "выводит прогресс");
            this.addCommand("Путешествие", puteshestvie, new List<String> { "Петушествие", "Зобиф", "Забив", "Зобив", "Приключение", "Поход", "Начало" }, "Отправляет на зобиф");
        }

        public override bool processMessage(VkNet.Model.Message message)
        {
            if (!processCommand(message))
            {
                vkapi.sendMessageToQuery(new MessagesSendParams
                {
                    UserId = message.FromId,
                    Message =  "Такой команды не существует"
                });
            }
            else
                return true;


            return false;
        }

        public void commListCommand(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            StringBuilder str = new StringBuilder("Вы можете воспользоваться этими командами. Для этого напишите мне команду или первую букву команды:\r\n\r\n");
            foreach (String name in this.getCommands()) str.AppendLine(name);
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = str.ToString(),
            });
            messageThread.fsm_state = 0;
        }
        public void puteshestvie(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            var users = vkapi.vkApi.Users.Get(new long[] { (long)message.FromId });
            string path_1 = Directory.GetCurrentDirectory() + "\\oper\\" + users[0].Id + "oper.txt";
            string path_2 = Directory.GetCurrentDirectory() + "\\save\\" + users[0].Id + "save.txt";
            if (!File.Exists(path_1) && !File.Exists(path_2))
            {
                File.CreateText(path_1);
                using (StreamWriter sw = File.AppendText(path_1))
                {
                    sw.WriteLine("+1");
                }
            }
            else if (File.Exists(path_2) && !File.Exists(path_1))
            {
                File.CreateText(path_1);
                using (StreamReader sr = File.OpenText(path_2)) {
                    using (StreamWriter sw = File.AppendText(path_1))
                    {
                        string s;
                        while ((s = sr.ReadLine()) != null)
                        {
                            sw.WriteLine(s);
                        }
                    }
                }
            }
            switch (messageThread.fsm_state)
            {
                case 0:
                    reply = "Куда вы хотите отправится?";
                    messageThread.fsm_state = 1;
                    vkapi.sendMessageToQuery(new MessagesSendParams
                    {
                        UserId = message.FromId,
                        Message = reply
                    });
                    break;
                case 1:
                    if (message.Text.Contains("Лес"))
                    {
                        reply = "На вас напал кабан";
                        messageThread.fsm_state = 2;
                        vkapi.sendMessageToQuery(new MessagesSendParams
                        {
                            UserId = message.FromId,
                            Message = reply
                        });
                    }
                    else if (message.Text.Contains("Горы"))
                    {
                        reply = "На вас напал одичалый";
                        messageThread.fsm_state = 3;
                        vkapi.sendMessageToQuery(new MessagesSendParams
                        {
                            UserId = message.FromId,
                            Message = reply
                        });
                    }
                    else if (message.Text.Contains("Пещера"))
                    {
                        reply = "На вас напал Арагог";
                        messageThread.fsm_state = 4;
                        vkapi.sendMessageToQuery(new MessagesSendParams
                        {
                            UserId = message.FromId,
                            Message = reply
                        });
                    }
                    else if (message.Text.Contains("Руины"))
                    {
                        reply = "На вас напал скелет-воин";
                        messageThread.fsm_state = 5;
                        vkapi.sendMessageToQuery(new MessagesSendParams
                        {
                            UserId = message.FromId,
                            Message = reply
                        });
                    }
                    break;
                case 2:
                    {
                        messageThread.fsm_state = 0;
                        int bonus_player = 0;
                        string s;
                        string alltext = "";
                        using (StreamReader sr = File.OpenText(path_1))
                        {
                            while ((s = sr.ReadLine()) != null)
                            {
                                try
                                {
                                    bonus_player += Convert.ToInt32(s);
                                    alltext += s;
                                }
                                catch { }
                            }
                        }
                        if (bonus_player >= 1)
                        {
                            reply = "YOU WON!";
                            vkapi.sendMessageToQuery(new MessagesSendParams
                            {
                                UserId = message.FromId,
                                Message = reply
                            });
                            using (StreamWriter sw = File.CreateText(path_1))
                            {
                                var hell = alltext.Split('\n');
                                double help = 0;
                                help += 0.01;
                                if (help >= 1)
                                {
                                    hell[0] = (Convert.ToInt32(hell[0]) + help).ToString();
                                    help--;
                                    sw.WriteLine(Convert.ToInt32(hell[0]));
                                }
                                Random rnd = new Random();
                                int hellp = rnd.Next(0, 999999999);
                                int hep = rnd.Next(0, 10);
                                if (hep == 10)
                                {
                                    reply = "Вам выпал napapijiri (бонус на забиф + 1)";
                                    vkapi.sendMessageToQuery(new MessagesSendParams
                                    {
                                        UserId = message.FromId,
                                        Message = reply
                                    });
                                    sw.WriteLine("+1");
                                }
                                if (hellp == 999999999)
                                {
                                    reply = "Вам выпал стоник (бонус на забиф + 100)";
                                    vkapi.sendMessageToQuery(new MessagesSendParams
                                    {
                                        UserId = message.FromId,
                                        Message = reply
                                    });
                                    hell[0] = (Convert.ToInt32(hell[0]) + 100).ToString();
                                    sw.WriteLine(hell);
                                }
                            }
                        }
                        else
                        {
                            reply = "YOU DIED!";
                            vkapi.sendMessageToQuery(new MessagesSendParams
                            {
                                UserId = message.FromId,
                                Message = reply
                            });
                            if (File.Exists(path_2))
                            {
                                using (StreamReader sr = File.OpenText(path_2))
                                {
                                    using (StreamWriter sw = File.AppendText(path_1))
                                    {
                                        while ((s = sr.ReadLine()) != null)
                                        {
                                            sw.WriteLine(s);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                File.CreateText(path_1);
                                using (StreamWriter sw = File.AppendText(path_1))
                                {
                                    sw.WriteLine("+1");
                                }
                            }
                        }
                    }
                    break;
                case 3:
                    {
                        messageThread.fsm_state = 0;
                        int bonus_player = 0;
                        string s;
                        string alltext = "";
                        using (StreamReader sr = File.OpenText(path_1))
                        {
                            while ((s = sr.ReadLine()) != null)
                            {
                                bonus_player += Convert.ToInt32(s);
                                alltext += s;
                            }
                        }
                        if (bonus_player >= 5)
                        {
                            reply = "YOU WON!";
                            vkapi.sendMessageToQuery(new MessagesSendParams
                            {
                                UserId = message.FromId,
                                Message = reply
                            });
                            using (StreamWriter sw = File.CreateText(path_1))
                            {
                                var hell = alltext.Split('\n');
                                double help = 0;
                                help += 0.1;
                                if (help >= 1)
                                {
                                    hell[0] = (Convert.ToInt32(hell[0]) + help).ToString();
                                    help--;
                                    sw.WriteLine(hell);
                                }
                                Random rnd = new Random();
                                int hellp = rnd.Next(0, 999999999);
                                int hep = rnd.Next(0, 10);
                                if (hep == 10)
                                {
                                    reply = "Вам выпал вещь (бонус на забиф + 3)";
                                    vkapi.sendMessageToQuery(new MessagesSendParams
                                    {
                                        UserId = message.FromId,
                                        Message = reply
                                    });
                                    sw.WriteLine("+3");
                                }
                                if (hellp == 999999999)
                                {
                                    reply = "Вам выпал стоник (бонус на забиф + 100)";
                                    vkapi.sendMessageToQuery(new MessagesSendParams
                                    {
                                        UserId = message.FromId,
                                        Message = reply
                                    });
                                    hell[0] = (Convert.ToInt32(hell[0]) + 100).ToString();
                                    sw.WriteLine(hell);
                                }
                            }
                        }
                        else
                        {
                            reply = "YOU DIED!";
                            vkapi.sendMessageToQuery(new MessagesSendParams
                            {
                                UserId = message.FromId,
                                Message = reply
                            });
                            if (File.Exists(path_2))
                            {
                                using (StreamReader sr = File.OpenText(path_2))
                                {
                                    using (StreamWriter sw = File.AppendText(path_1))
                                    {
                                        while ((s = sr.ReadLine()) != null)
                                        {
                                            sw.WriteLine(s);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                File.CreateText(path_1);
                                using (StreamWriter sw = File.AppendText(path_1))
                                {
                                    sw.WriteLine("+1");
                                }
                            }
                        }
                    }
                    break;
                case 4:
                    {
                        messageThread.fsm_state = 0;
                        int bonus_player = 0;
                        string s;
                        string alltext = "";
                        using (StreamReader sr = File.OpenText(path_1))
                        {
                            while ((s = sr.ReadLine()) != null)
                            {
                                bonus_player += Convert.ToInt32(s);
                                alltext += s;
                            }
                        }
                        if (bonus_player >= 10)
                        {
                            reply = "YOU WON!";
                            vkapi.sendMessageToQuery(new MessagesSendParams
                            {
                                UserId = message.FromId,
                                Message = reply
                            });
                            using (StreamWriter sw = File.CreateText(path_1))
                            {
                                var hell = alltext.Split('\n');
                                double help = 0;
                                help += 0.3;
                                if (help >= 1)
                                {
                                    hell[0] = (Convert.ToInt32(hell[0]) + help).ToString();
                                    help--;
                                    sw.WriteLine(hell);
                                }
                                Random rnd = new Random();
                                int hellp = rnd.Next(0, 999999999);
                                int hep = rnd.Next(0, 10);
                                if (hep == 10)
                                {
                                    reply = "Вам выпал мечЪ (бонус на забиф + 5)";
                                    vkapi.sendMessageToQuery(new MessagesSendParams
                                    {
                                        UserId = message.FromId,
                                        Message = reply
                                    });
                                    sw.WriteLine("+5");
                                }
                                if (hellp == 999999999)
                                {
                                    reply = "Вам выпал стоник (бонус на забиф + 100)";
                                    vkapi.sendMessageToQuery(new MessagesSendParams
                                    {
                                        UserId = message.FromId,
                                        Message = reply
                                    });
                                    hell[0] = (Convert.ToInt32(hell[0]) + 100).ToString();
                                    sw.WriteLine(hell);
                                }
                            }
                        }
                        else
                        {
                            reply = "YOU DIED!";
                            vkapi.sendMessageToQuery(new MessagesSendParams
                            {
                                UserId = message.FromId,
                                Message = reply
                            });
                            if (File.Exists(path_2))
                            {
                                using (StreamReader sr = File.OpenText(path_2))
                                {
                                    using (StreamWriter sw = File.AppendText(path_1))
                                    {
                                        while ((s = sr.ReadLine()) != null)
                                        {
                                            sw.WriteLine(s);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                File.CreateText(path_1);
                                using (StreamWriter sw = File.AppendText(path_1))
                                {
                                    sw.WriteLine("+1");
                                }
                            }
                        }
                    }
                    break;
                case 5:
                    {
                        messageThread.fsm_state = 0;
                        int bonus_player = 1;
                        string s;
                        string alltext = "";
                        using (StreamReader sr = File.OpenText(path_1))
                        {
                            while ((s = sr.ReadLine()) != null)
                            {
                                bonus_player += Convert.ToInt32(s);
                                alltext += s;
                            }
                        }
                        if (bonus_player >= 15)
                        {
                            reply = "YOU WON!";
                            vkapi.sendMessageToQuery(new MessagesSendParams
                            {
                                UserId = message.FromId,
                                Message = reply
                            });
                            using (StreamWriter sw = File.CreateText(path_1))
                            {
                                var hell = alltext.Split('\n');
                                double help = 0;
                                help += 0.5;
                                if (help >= 1)
                                {
                                    hell[0] = (Convert.ToInt32(hell[0]) + help).ToString();
                                    help--;
                                    sw.WriteLine(hell);
                                }
                                Random rnd = new Random();
                                int hellp = rnd.Next(0, 999999999);
                                int hep = rnd.Next(0, 10);
                                if (hep == 10)
                                {
                                    reply = "Вам выпал shitЪ (бонус на забиф + 7)";
                                    vkapi.sendMessageToQuery(new MessagesSendParams
                                    {
                                        UserId = message.FromId,
                                        Message = reply
                                    });
                                    sw.WriteLine("+7");
                                }
                                if (hellp == 999999999)
                                {
                                    reply = "Вам выпал стоник (бонус на забиф + 100)";
                                    vkapi.sendMessageToQuery(new MessagesSendParams
                                    {
                                        UserId = message.FromId,
                                        Message = reply
                                    });
                                    hell[0] = (Convert.ToInt32(hell[0]) + 100).ToString();
                                    sw.WriteLine(hell);
                                }
                            }
                        }
                        else
                        {
                            reply = "YOU DIED!";
                            vkapi.sendMessageToQuery(new MessagesSendParams
                            {
                                UserId = message.FromId,
                                Message = reply
                            });
                            if (File.Exists(path_2))
                            {
                                using (StreamReader sr = File.OpenText(path_2))
                                {
                                    using (StreamWriter sw = File.AppendText(path_1))
                                    {
                                        while ((s = sr.ReadLine()) != null)
                                        {
                                            sw.WriteLine(s);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                File.CreateText(path_1);
                                using (StreamWriter sw = File.AppendText(path_1))
                                {
                                    sw.WriteLine("+1");
                                }
                            }
                        }
                    }
                    break;
            }
            reply = "";
        }
        public void stata(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            String reply = "";
            var users = vkapi.vkApi.Users.Get(new long[] { (long)message.FromId });
            string path_1 = Directory.GetCurrentDirectory() + "\\oper\\" + users[0].Id + "oper.txt";
            string path_2 = Directory.GetCurrentDirectory() + "\\save\\" + users[0].Id + "save.txt";
            if (!File.Exists(path_1))
            {
                reply = "У тебя пока нет статистики";
                vkapi.sendMessageToQuery(new MessagesSendParams
                {
                    UserId = message.FromId,
                    Message = reply
                });
                messageThread.fsm_state = 0;
            }
            else if (File.Exists(path_1))
            {
                using (StreamReader sr = File.OpenText(path_1))
                {
                    string s;
                    while ((s = sr.ReadLine()) != null)
                    {
                        reply += s + "\n";
                    }
                }
                vkapi.sendMessageToQuery(new MessagesSendParams
                {
                    UserId = message.FromId,
                    Message = "Твой бонус:\n" + reply
                });
            }
            else if (!File.Exists(path_2))
            {
                reply = "У тебя пока нет сохранённой статистики";
                vkapi.sendMessageToQuery(new MessagesSendParams
                {
                    UserId = message.FromId,
                    Message = reply
                });
            }
            messageThread.fsm_state = 0;
        }
        public void addiction(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply
            });
            messageThread.fsm_state = 0;
        }
        public void save(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            var users = vkapi.vkApi.Users.Get(new long[] { (long)message.FromId });
            string path_1 = Directory.GetCurrentDirectory() + "\\oper\\" + users[0].Id + "oper.txt";
            string path_2 = Directory.GetCurrentDirectory() + "\\save\\" + users[0].Id + "save.txt";
            if (!File.Exists(path_2))
            {
                File.CreateText(path_2);
                using (StreamReader sr = File.OpenText(path_1))
                {
                    using (StreamWriter sw = File.AppendText(path_2))
                    {
                        string s;
                        while ((s = sr.ReadLine()) != null)
                        {
                            sw.WriteLine(s);
                        }
                    }
                }
            }
            else if (File.Exists(path_2))
            {
                using (StreamReader sr = File.OpenText(path_1))
                {
                    using (StreamWriter sw = File.AppendText(path_2))
                    {
                        string s;
                        while ((s = sr.ReadLine()) != null)
                        {
                            sw.WriteLine(s);
                        }
                    }
                }
            }
            reply = "Сохранено.";
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply
            });
            messageThread.fsm_state = 0;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Utils;
using VkNet.Utils.AntiCaptcha;
using VkNet.Model.RequestParams;
using SkBot.utilities;
using System.Text.RegularExpressions;
using System.Globalization;

using OfficeOpenXml;
using System.Net;
using System.Net.Mail;
using System.IO;


namespace SkBot
{
    class Users : MessageProcessor
    {
        int herohp = 0;
        int monsterhp = 0;
        int healpotion = 0;
        public Users(ref VkApiWithQuery _vk) : base(ref _vk)
        {
            this.addCommand("Команды", commListCommand, new List<String> { "Команда", "Список команд", "К", "R" }, "выводит список доступных команд");
            this.addCommand("Привет", privet, new List<String> { "Здравствуй", "Приветик" });
            this.addCommand("Пока", poka, new List<String> { "До свидания" });
            this.addCommand("Что ты делаешь?", chtodela);
            this.addCommand("Как дела?", kakdela);
            this.addCommand("Жанры", ganr, new List<String> { "Ж" }, "Выдаёт мультики по жанрам");
            this.addCommand("Персонажи", characters, new List<String> { "П" }, "Присылает информацию о персонажах");
            this.addCommand("Сортировка", sorting, new List<String> { "С", "Сортировки", "Сортировать", "Сорт" }, "Сортирует Мультики");
            this.addCommand("Мультик", cartoon, new List<String> { "М", "Мультфильм", "Мульт", "Мультфильмы", "Мультики", "Мульты" }, "Присылает ссылку на мультик");
            this.addCommand("Что есть посмотреть?", posmotret, new List<String> { "Ч" }, "Выдаёт мультик в соответствии с вашими предпочтниями");
            this.addCommand("Факт", fact, new List<String> { "Ф", "Факты", "Фактик" }, "Присылает случайный факт о мультфильмах");
            this.addCommand("Игра", bamaga, new List<String> { "И", "Игры" }, "Игра в камень ножницы бумага");
           // this.addCommand(".", SpamLis, new List<string>(), "");
            this.addCommand("РПГ", velikafigna, new List<string> {"Р"}, "Короткая РПГ игра");
        }

        public override bool processMessage(VkNet.Model.Message message)
        {
           // if(message.FromId == 2116884)
            //{
                //for (int i = 0; i < 11; i++)
                //{
                    //vkapi.sendMessageToQuery(new MessagesSendParams
                    //{
                       // UserId = message.FromId,
                       // Message = "Пепяка"
                   // });
                //}
           // }
            if (!processCommand(message))
            {
                vkapi.sendMessageToQuery(new MessagesSendParams
                {
                    UserId = message.FromId,
                    Message =  "Такой команды не существует, напиши мне Команды или К, чтобы узнать, что я умею"
                });
            }
            else
                return true;


            return false;
        }

        public void commListCommand(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {

            StringBuilder str = new StringBuilder("Вы можете воспользоваться этими командами. Для этого напишите мне команду или первую букву последних 6 команд:\r\n\r\n");
            foreach (String name in this.getCommands()) str.AppendLine(name);
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = str.ToString(),
            });
            messageThread.fsm_state = 0;
        }

       // public void SpamLis(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        //{
            
               // vkapi.sendMessageToQuery(new MessagesSendParams
               // {
                    //UserId = message.FromId,
                    //Message = "Вы лучший эксперт"
                //});
      //  }

        
        public void privet(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            // string reply = "";
            // switch (messageThread.fsm_state)
            // {
            // case 0:
            // messageThread.fsm_state = 1;
            //reply = "Хочешь Обнимашек?";
            //break;
            //case 1:
            //messageThread.fsm_state = 0;
            //reply = "Ура, я люблю обнимашки!!!";
            //break;
            //}                    
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = "Привет-Привет!!!"
           });
        }
        public void poka(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = "Пока-Пока, приходи ещё",
            });
        }
        public void chtodela(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = "Думаю, что можно предложить вам посмотреть сегодня",
            });
        }
        public void kakdela(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = "Отлино, чувствую себя мультяшно!!!",
            });
        }
        public void ganr(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            String reply = "Вот список доступных жанров\r\n";
            string title = "";
                
            //Привет Алёне от Сергея Сергеевича

            switch (messageThread.fsm_state)
            {
                case 0:
                    
                    List<string> genres = new List<string>();
                    messageThread.fsm_state = 1;
                    for (int i = 1; i < 5; i++)
                    {


                        WebRequest request = WebRequest.Create("https://www.kinopoisk.ru/top/lists/13/filtr/all/sort/order/genre[14]/14/page/" + i + "/");
                        request.Method = "GET";
                        using (WebResponse response = request.GetResponse())
                        {
                            Stream stream = response.GetResponseStream();
                            using (StreamReader reader = new StreamReader(stream))
                            {
                                string content = reader.ReadToEnd();

                                Match m = Regex.Match(content, "мультфильм,.+[...]");
                                while (m.Success)
                                {
                                    if (!genres.Contains(m.Value))
                                    {
                                        genres.Add(m.Value);
                                    }
                                    m = m.NextMatch();
                                }
                            }
                        }
                    }
                    genres.Sort();
                    messageThread.arg = genres;
                    foreach (var genre in genres)
                    {
                        reply += genre + "\r\n";
                    }
                    break;
                case 1:
                    string search_string;
                    genres = messageThread.arg as List<string>;
                    if (genres.Contains(message.Text)) search_string = genres.Find(x => x == message.Text);
                    else search_string = genres.Find(x => x.Contains(message.Text));
                    messageThread.fsm_state = 0;
                    for (int i =1; i<=5; i++)
                    {
                        WebRequest request = WebRequest.Create("https://www.kinopoisk.ru/top/lists/13/filtr/all/sort/order/genre[14]/14/page/" + i + "/");
                        request.Method = "GET";
                        using (WebResponse response = request.GetResponse())
                        {
                            Stream stream = response.GetResponseStream();
                            using (StreamReader reader = new StreamReader(stream))
                            {
                                string content = reader.ReadToEnd();

                                Match m = Regex.Match(content, message.Text);
                                while (m.Success)
                                {
                                   // Match e = Regex.Match(content, "<dt>Жанры:</dt>");
                                   // Match r = Regex.Match(content, "</html>");
                                    //Math t = Regex.Match(content, "<div class=\"ratingBlock  ratingGreenBG\">");
                                   // Match y = Regex.Match(content.Remove(0, t.Index + e.Index, r.Index,), message.Text);

                                    Match n = Regex.Match(content.Remove(0, m.Index), "film\\/\\d+");
                                    Match q = Regex.Match(content.Remove(0, m.Index), "data-title=\".+\"");
                                    string name = q.Value;
                                    title = name.Remove(0, name.IndexOf("\""));
                                    reply += "https://www.kinopoisk.ru/" + n.Value + " " + title + "\n";
                                    m = m.NextMatch();
                                }
                            }
                        }

                    }
                    if (reply == "") reply = "Мы ничего не нашли";
                    break;
            }
   

            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply,
            });
        }
        public void characters(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            int fun = 0;
            string reply = "";
            string path = "";
            string[] files;
            IReadOnlyCollection<VkNet.Model.Attachments.Photo> photos = null;
            switch (messageThread.fsm_state)
            {
                case 0:
                    fun = 1;
                    messageThread.fsm_state = 1;
                    reply = "Напиши мне имя персонажа, и я выдам информацию про него, вот спиок доступных персонажей\n";
                    path = Directory.GetCurrentDirectory() + "\\Персонажи";
                    files = Directory.GetFiles(path);
                    foreach (var file in files)
                    {
                        reply += file.Remove(0, path.Length + 1).Replace(".txt", "") + "\n";
                    }
                    break;
                case 1:
                    fun = 0;
                    path = Directory.GetCurrentDirectory() + "\\Персонажи";
                    files = Directory.GetFiles(path);
                    foreach(var file in files)
                    {
                        if (file.ToLower().Contains(message.Text.ToLower()))
                        {
                            reply = File.ReadAllText(file, Encoding.Default);
                            fun = 1;

                        }
                        
                    }
                    path = Directory.GetCurrentDirectory() + "\\Персонажи Фотки";
                    files = Directory.GetFiles(path);
                    foreach(var file in files)
                    {
                        var charName = file.Remove(file.Length - 3, 3);
                        if (charName.ToLower().Contains(message.Text.ToLower()))
                        {
                            using (WebClient webClient = new WebClient())
                            {

                                // Получить адрес сервера для загрузки.
                                var uploadServer = vkapi.vkApi.Photo.GetMessagesUploadServer((long)message.PeerId);
                                // Загрузить файл.
                                var responseFile = Encoding.ASCII.GetString(webClient.UploadFile(uploadServer.UploadUrl, file));
                                // Сохранить загруженный файл
                                photos = vkapi.vkApi.Photo.SaveMessagesPhoto(responseFile);
                            }

                        }                       
                    }
                    messageThread.fsm_state = 0;

                    break;
            }
            if (fun==0)
            {
                reply = "Персонаж не был найден";
            }
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply,
                Attachments = photos
            });
      }
        public void sorting(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            string reply = "";
          
          
                string path = Directory.GetCurrentDirectory() + "\\sorts.txt";
                if (File.Exists(path))
                {
                reply = "Выберите метод сортировки, и я выдам список мультиков отортированных по нему\n" + File.ReadAllText(path, Encoding.Default);
                }
                else
                 {
                reply = "Что то пошло не так";
                 }

                vkapi.sendMessageToQuery(new MessagesSendParams
                {
                    UserId = message.FromId,
                    Message = reply,
                });
            }
       
        public void cartoon(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            IReadOnlyCollection<VkNet.Model.Attachments.Photo> photos = null;
            string reply = "";
            switch (messageThread.fsm_state)
            {
                case 0:
                    messageThread.fsm_state = 1;
                    reply = "Напишите мне название мультика, и я выдам вам информацию про него";
                        break;
                case 1:
                    messageThread.fsm_state = 0;
                    WebRequest request = WebRequest.Create("https://www.kinopoisk.ru/index.php?kp_query=" + message.Text + "&what=");
                    request.Method = "GET";
                    using (WebResponse response = request.GetResponse())
                    {
                        Stream stream = response.GetResponseStream();
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            string content = reader.ReadToEnd();

                            Match m = Regex.Match(content, "Скорее всего, вы ищете:");
                            if (!m.Success)
                            {
                                reply = "По вашему запрос ничего не было найдено";
                                messageThread.fsm_state = 0;
                                break;
                           }
                            Match n = Regex.Match(content.Remove(0, m.Index), "film\\/\\d+");
                            WebRequest request2 = WebRequest.Create("https://www.kinopoisk.ru/" + n.Value);
                            request2.Method = "GET";
                            using (WebResponse response2 = request2.GetResponse())
                            {
                                Stream stream2 = response2.GetResponseStream();
                                using (StreamReader reader2 = new StreamReader(stream2))
                                {
                                    string content2 = reader2.ReadToEnd();
                                    Match q = Regex.Match(content2, "openImgPopup");
                                    Match w = Regex.Match(content2.Remove(0, q.Index), "https.+jpg");
                                    using (WebClient webClient = new WebClient())
                                    {

                                        webClient.DownloadFile(w.Value, Directory.GetCurrentDirectory() + "\\1.jpg");
                                        var uploadServer = vkapi.vkApi.Photo.GetMessagesUploadServer((long)message.PeerId);                                        
                                        var responseFile = Encoding.ASCII.GetString(webClient.UploadFile(uploadServer.UploadUrl, Directory.GetCurrentDirectory() + "\\1.jpg"));
                                        photos = vkapi.vkApi.Photo.SaveMessagesPhoto(responseFile);
                                        File.Delete(Directory.GetCurrentDirectory() + "1.jpg");
                                    }
                                }
                            }
                            reply = "https://www.kinopoisk.ru/" + n.Value;
                        }
                    }
                    break;
            }         
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply,
                Attachments = photos
            });
        }
        public void posmotret(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {           
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = "",
            });
        }
        public void fact(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            string path = Directory.GetCurrentDirectory() + "\\facts.txt";
            string text = File.ReadAllText(path, Encoding.Default);
            var strings = text.Split(new char[] { '\n' });
            Random r = new Random();
            String reply = strings[r.Next(strings.Length - 1)];
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply,
            });
        }
        public void bamaga(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            string reply = "";
            string game = message.Text;
            Random rnd = new Random();
            int hell = rnd.Next(1, 3);
            int heaven = 0;
            switch (messageThread.fsm_state)
            {
                case 0:
                    messageThread.fsm_state = 1;
                    reply = "Камень\nНожницы\nБумага";
                        break;
                case 1:
                    messageThread.fsm_state = 2;
                    if (game.ToLower() == "бумага")
                    {
                        heaven = 1;
                    }
                    else if (game.ToLower() == "камень")
                    {
                        heaven = 2;
                    }
                    else if (game.ToLower() == "ножницы")
                    {
                        heaven = 3;
                    }
                    else
                    {
                        reply = "Всё плохо";
                        messageThread.fsm_state = 0;
                        break;

                    }
                    if (hell == 1)
                    {
                        reply = "Бумага";
                    }
                    else if (hell == 2)
                    {
                        reply = "Камень";
                    }
                    else if (hell == 3)
                        {
                        reply = "Ножницы";
                    }
               
                    messageThread.fsm_state = 0;
                    if ((hell == 1) && (heaven == 2))
                    {
                        reply = reply + "\nВы проиграли";
                    }
                    else if ((hell == 1) && (heaven == 3))
                    {
                        reply = reply + "\nВы выиграли";
                    }
                    else if ((hell == 1) && (heaven == 1))
                    {
                        reply = reply + "\nНичья";
                    }
                    else if ((hell == 2) && (heaven == 1))
                    {
                        reply = reply + "\nВы выиграли";
                    }
                    else if ((hell == 2) && (heaven == 2))
                    {
                        reply = reply + "\nНичья";
                    }
                    else if ((hell == 2) && (heaven == 3))
                    {
                        reply = reply + "\nВы проиграли";
                    }
                    else if ((hell == 3) && (heaven == 1))
                    {
                        reply = reply + "\nВы проиграли";
                    }
                    else if ((hell == 3) && (heaven == 2))
                    {
                        reply = reply + "\nВы выиграли";
                    }
                    else if ((hell == 3) && (heaven == 3))
                    {
                        reply = reply + "\nНичья";
                    }
                    break;
               
                   
                   
            }

    
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply,
            });
        }
        public void velikafigna(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {        
            string reply = "";  
            switch (messageThread.fsm_state)
            {
                case 0:
                    reply = "Игра Началась, пишите аттака, чтобы атаковать или лечение, чтобы подлечиться";
                    messageThread.fsm_state = 1;
                     herohp = 100;
                     monsterhp = 100;
                    healpotion = 3;
                    break;
                case 1:

                    Random rnd = new Random();
                    int heroattack = rnd.Next(1, 30);                 
                    int monsterattack = rnd.Next(1, 30);
                    int monsterhealchance = rnd.Next(1, 10);
                    if (message.Text.ToLower() == "аттака")
                    {
                        if (heroattack > monsterattack)
                        {
                            monsterhp = monsterhp - heroattack;
                            if ((herohp <= 0) || (monsterhp <= 0))
                            {
                                reply = "";
                            }
                            else
                            {
                                reply = "Вы аттакуете\n" + "Жизни монстра = " + monsterhp;
                            }
                        }
                        else if (heroattack < monsterattack)
                        {
                            herohp = herohp - monsterattack;
                            if ((herohp <= 0) || (monsterhp <= 0))
                            {
                                reply = "";
                            }
                            else
                            {
                                reply = "Монстр аттакует\n" + "Ваши жизни = " + herohp;
                            }

                        }
                        else if (heroattack == monsterattack)
                        {
                            reply = "мимо";
                        }
                      
                   }
                    else if ((message.Text.ToLower() == "лечение") && (herohp>0))
                    {
                        herohp = herohp + 20;
                        healpotion = healpotion - 1;
                        reply = "Вы подлечились\n" + "Ваши Жизни = " + herohp + "\n" + "Количество оставшихся зелий = " + healpotion;
                    }
                    else
                    {
                        reply = "Я тебя не понимать";
                        messageThread.fsm_state = 0;
                        break;
                    }
                    if ((herohp <= 0) || (monsterhp <= 0))
                    {
                        messageThread.fsm_state = 0;
                        if (herohp <= 0)
                        {
                            reply = "вы проиграли";
                        }
                        else if (monsterhp <= 0)
                        {
                            reply = "Вы выиграли";
                        }
                        break;
                    }
                    if ((monsterhealchance>8) && (monsterhp>0))
                    {
                        monsterhp = monsterhp + 10;
                        reply = reply + "\n" + "Монстр Подлечился\n" + "Его жизни = " + monsterhp;
                    }
                    break;             
            }                  
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply,
            }) ; 
        }
    }
    }

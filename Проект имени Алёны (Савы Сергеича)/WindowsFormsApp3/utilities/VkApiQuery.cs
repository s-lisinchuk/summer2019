﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Utils;
using VkNet.Utils.AntiCaptcha;
using VkNet.Model.RequestParams;

namespace SkBot.utilities
{
    class VkApiWithQuery
    {
        
        public VkApi vkApi;
        readonly List<MessagesSendParams> messageQuery;
        int lifes;
        private long last_time_used;
        int numberOfLifes;
        Random r = new Random();
        public VkApiWithQuery(int numberOfRequests, ref VkApi _vkApi)
        {
            vkApi = _vkApi;
            messageQuery = new List<MessagesSendParams>();
            lifes = numberOfRequests;
            numberOfLifes = numberOfRequests;
        }

        public void sendMessageToQuery(MessagesSendParams @params)
        {
            @params.RandomId = r.Next();
            messageQuery.Add(@params);
        }

        public void processQuery()
        {
            if (DateTime.Now.Ticks > last_time_used + 10000000)
            {
                lifes = numberOfLifes;
                last_time_used = DateTime.Now.Ticks;
            }
            if ((messageQuery.Count != 0) && (lifes > 0))
            {
                lifes--;
                if (messageQuery[0].UserId != null)
                {
                    try
                    {
                        if (messageQuery[0].Message.Length > 4096)
                        {
                            String longmessage;
                            var mess = messageQuery[0];
                            longmessage = mess.Message;
                            mess.Message = messageQuery[0].Message.Remove(4095);
                            vkApi.Messages.Send(mess);
                            mess.Message = longmessage.Remove(0, 4095);
                            vkApi.Messages.Send(messageQuery[0]);
                        }
                        else
                        {
                            vkApi.Messages.Send(messageQuery[0]);
                        }
                        messageQuery.RemoveAt(0);
                    }
                    catch(Exception e)
                    {
                        messageQuery.RemoveAt(0);
                    }
                    return;
                }
                if (messageQuery[0].UserIds != null)
                {
                    try
                    {
                       
                        vkApi.Messages.SendToUserIds(messageQuery[0]);
                        messageQuery.RemoveAt(0);
                    }
                    catch
                    {
                        messageQuery.RemoveAt(0);
                    }
                    return;
                }
                if (messageQuery[0].ChatId != null)
                {
                    try
                    {
                        vkApi.Messages.Send(messageQuery[0]);
                        messageQuery.RemoveAt(0);
                    }
                    catch
                    {
                        messageQuery.RemoveAt(0);
                    }
                    return;
                }

            }
        }
    }
}

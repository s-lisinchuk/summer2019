﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace SkBot
{
    public class Commandsclass
    {
        readonly private List<Command> commandlist = new List<Command>();
        readonly private List<Messagethread> threadlist = new List<Messagethread>();
        public class Messagethread
        {
            public long? userid { get; set; }
            public string commandname { get; set; }
            public int fsm_state { get; set; }
            public object arg { get; set; }

            public Messagethread(long? _userid, string _commandname, int _fsm_state, object _arg)
            {
                userid = _userid;
                commandname = _commandname;
                fsm_state = _fsm_state;
                arg = _arg;
            }
            public Messagethread(long? _userid, string _commandname, int _fsm_state)
            {
                userid = _userid;
                commandname = _commandname;
                fsm_state = _fsm_state;
            }
        }
        public delegate void commandReceivedCallback(VkNet.Model.Message message, Commandsclass.Messagethread messageThread);
        private class Command
        {
            public String commandname { get; }
            public commandReceivedCallback callback { get; }
            readonly private List<String> synonyms = new List<string>();
            public String description { get; }
            public Command(String _commandname, commandReceivedCallback _callback)
            {
                commandname = _commandname;
                callback = _callback;
            }
            public Command(String _commandname, commandReceivedCallback _callback, List<String> _synonyms)
            {
                commandname = _commandname;
                callback = _callback;
                synonyms.AddRange(_synonyms);
            }
            public Command(String _commandname, commandReceivedCallback _callback, List<String> _synonyms, String _description)
            {
                commandname = _commandname;
                callback = _callback;
                synonyms.AddRange(_synonyms);
                description = _description;
            }
            public bool isCommand(String messagetext)
            {
                if ((messagetext.StartsWith((commandname), StringComparison.CurrentCultureIgnoreCase))) return true;
                else foreach (var synonym in synonyms)
                    {
                        if (Regex.IsMatch(messagetext, "^" + synonym + "\\W", RegexOptions.IgnoreCase)) return true;
                        if (Regex.IsMatch(messagetext, "^" + synonym + "$", RegexOptions.IgnoreCase)) return true;
                    }
                return false;
            }
        }

        public void addCommand(String _commandname, commandReceivedCallback _callback)
        {
            Command newcomm = new Command(_commandname, _callback);
            commandlist.Add(newcomm);
        }

        public void addCommand(String _commandname, commandReceivedCallback _callback, List<String> synonyms)
        {
            Command newcomm = new Command(_commandname, _callback, synonyms);
            commandlist.Add(newcomm);
        }
        public void addCommand(String _commandname, commandReceivedCallback _callback, List<String> _synonyms, String _description)
        {
            Command newcomm = new Command(_commandname, _callback, _synonyms, _description);
            commandlist.Add(newcomm);
        }

        public List<String> getCommands()
        {
            List<String> commandnamelist = new List<string>();
            foreach (Command comm in commandlist)
            {
                if (comm.description != null)
                    commandnamelist.Add(comm.commandname + " - " + comm.description + "\r\n\r\n");
                else
                    commandnamelist.Add(comm.commandname + "\r\n");
            }
            return commandnamelist;
        }

        public bool parseMessageText(VkNet.Model.Message message)
        {
            if (threadlist.Exists(x => (x.userid == message.FromId)))
            {
                Messagethread thread = threadlist.Find(x => x.userid == message.FromId);
                Command comm = commandlist.Find(x => x.commandname.Equals((thread.commandname), StringComparison.CurrentCultureIgnoreCase));
                comm.callback(message, thread);
                if (thread.fsm_state == 0) threadlist.Remove(thread);
                return true;
            } else
            {
                foreach (Command comm in commandlist)
                {
                    if (comm.isCommand(message.Text))
                    {
                        Messagethread thread = new Messagethread(message.FromId, comm.commandname, 0);
                        threadlist.Add(thread);
                        comm.callback(message, thread);
                        if (thread.fsm_state == 0) threadlist.RemoveAll(x => x.userid == message.FromId);
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

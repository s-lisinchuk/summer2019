﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VkNet;
using VkNet.Categories;
using VkNet.Enums.Filters;
using VkNet.Utils;
using VkNet.Utils.AntiCaptcha;
using VkNet.Model.RequestParams;
using SkBot.utilities;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net;
using System.IO;

namespace SkBot
{
    class Users : MessageProcessor
    {
        
        public Users(ref VkApiWithQuery _vk) : base(ref _vk)
        {
            this.addCommand("Команды", commListCommand, new List<String> { "Команда", "Список команд", "К", "R" }, "выводит список доступных команд");
            this.addCommand("Привет", privet, new List<String> { "Прив", "ку", "hi", "privet" });
            this.addCommand("Пока", poka, new List<String> { "Пока", "Покет", "Споки ноки", "bye" });
            this.addCommand("Фотку", getphoto, new List<String> { "фотку", "фото", "photo" });
            this.addCommand("Загрузить фотку", savephoto); 
            
        }
        
        public override bool processMessage(VkNet.Model.Message message)
        {
            if (!processCommand(message))
            {
                vkapi.sendMessageToQuery(new MessagesSendParams
                {
                    UserId = message.FromId,
                    Message =  "Не шали!!!)"
                });
            }
            else
                return true;


            return false;
        }

        public void commListCommand(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            StringBuilder str = new StringBuilder("Вы можете воспользоваться этими командами. Для этого напишите мне команду или первую букву команды:\r\n\r\n");
            foreach (String name in this.getCommands()) str.AppendLine(name);
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = str.ToString(),
            });
            messageThread.fsm_state = 0;
        }

        public void privet(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = "Ку,анимешник(ца)",
                
            });
            messageThread.fsm_state = 0;
        }

        public void poka(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            String reply = "";
            switch (messageThread.fsm_state)
            {
                case 0:
                    reply = "ББ,анимешник(ца)";
                    messageThread.fsm_state = 1;
                    break;
                case 1:
                    reply ="Я не понимаю!!!";
                    messageThread.fsm_state = 0;
                    break;
            }
            vkapi.sendMessageToQuery(new MessagesSendParams
            {
                UserId = message.FromId,
                Message = reply,

            });
        }

        public void getphoto(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            Random r = new Random();
            

            using (WebClient webClient = new WebClient())
            {
                String dir = "C:\\Users\\ZK\\Documents\\ALEX\\WindowsFormsApp3\\bin\\Debug\\pics\\";
                //Смотрим, где мы есть
                //И сохраняем фотку, которую нам прислали
                string[] files = Directory.GetFiles(dir);

                // Получить адрес сервера для загрузки.
                var uploadServer = vkapi.vkApi.Photo.GetMessagesUploadServer((long)message.PeerId);
                // Загрузить файл.
                var responseFile = Encoding.ASCII.GetString(webClient.UploadFile(uploadServer.UploadUrl, files[r.Next(files.Length - 1)]));
                // Сохранить загруженный файл
                var photos = vkapi.vkApi.Photo.SaveMessagesPhoto(responseFile);
                vkapi.sendMessageToQuery(new MessagesSendParams
                {
                    Attachments = photos,
                    UserId = message.FromId,
                    Message = "Лови фотку",
                });
            }
            messageThread.fsm_state = 0;
        }

        public void savephoto(VkNet.Model.Message message, Commandsclass.Messagethread messageThread)
        {
            String reply = "23";
            
            if ((340167726  == message.FromId) || (521014183 == message.FromId))
                {
                Random r = new Random();

                
                if (message.Attachments.Count == 0)
                {
                    reply = "А где картинка?";
                }
                else
                {
                    reply = "Готово модерарчик)";


                    using (WebClient webClient = new WebClient())
                    {
                        var photo = (message.Attachments[0].Instance as VkNet.Model.Attachments.Photo);
                        int max_size = FindMaxQual(photo.Sizes);
                        webClient.DownloadFile(photo.Sizes[max_size].Url, Directory.GetCurrentDirectory() + "\\pics\\" + photo + ".jpg");
                    }
                }
            }
            else
            {
                reply = "Только модераторы могут загружать фотки)";
            }
            vkapi.sendMessageToQuery(new MessagesSendParams
            {

                UserId = message.FromId,
                Message = reply,
            });
            messageThread.fsm_state = 0;
        }

        public int FindMaxQual(System.Collections.ObjectModel.ReadOnlyCollection<VkNet.Model.PhotoSize> Sizes)
        {
            ulong maximum = 0;
            int max_index = 0;
            for(int i = 0; i < Sizes.Count; i++)
            {
                if((Sizes[i].Height * Sizes[i].Width) > maximum)
                {
                    maximum = Sizes[i].Height * Sizes[i].Width;
                    max_index = i;
                }
            }
            return max_index;
        }
    }
}